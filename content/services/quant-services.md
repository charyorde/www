---
title: "Quant Services"
date: 2020-03-19T18:41:46+10:00
draft: false
featured: true
weight: 1
---

With our vast technological skillsets, tools made available to us through our
IBM Partnership, we’re able to see the business world through the eyes of data.
We therefore make informed and highly sophisticated decisions by leveraging on
wealth of data at our disposal.

We apply quantitative strategies to research, asset management, risk, modeling,
pricing and valuation.



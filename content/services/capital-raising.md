---
title: "Capital Raising"
date: 2020-03-19T18:33:46+10:00
draft: false
featured: true
weight: 1
---

Debt or Equity, our goal is to help well run and established businesses that
conform to our strategy raise capital. We understand that capital is the oil
that gets a company going thereby create employment and reduce the skill gaps.


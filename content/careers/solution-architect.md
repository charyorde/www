---
title: "Solution Architect"
date: 2022-12-20T10:08:46+10:00
draft: false
featured: true
listid: accordion-5294-item-3
heading: heading3
jobid: hjid-02-solutions
joblevel: Experienced
weight: 1
---
Position Summary

This position is responsible for providing end to end architecture strategies and solutions for our Customers.

As an Architect you will be expected to:
- Deliver high quality architecture solutions for large multi-generational programs spanning one or more business domains to support organizational missions
- Adhere to and help create enterprise principles and standards based on industry best practices
- Work on multiple projects utilizing agile methodologies to provide target-state solutions that best fit the business requirements and align to enterprise goals
- Create and maintain the target application architecture and blueprints along with strategic roadmaps
- Maintain reference architecture and defines shared application components, platforms, interfaces, and development tools
- Develop prototypes of the system design and work with database, operations, technical support and other IT areas as appropriate throughout development and implementation process
- Plan and lead proof of concepts (POCs) in support of new technology and/or new design approaches
- Provide high-level design choices and recommendations based on architecture alternative analysis
- Drive buy vs build decisions based on reviewing products and capabilities within the enterprise as well as leading products and technologies in the industry
- Effectively communicate designs and solutions in multiple forums and to various audiences including technology and business executives.
- Collaborate with other architecture resources within the organization and enterprise
- Offer technical guidance on technical standards, including coding standards, tools, or platforms

Required Skills

- Prior experience in digitalization and automation solutions for front and back office operations
- Design and development experience in modern platforms using MuleSoft AnyPoint platform, REST/API integration, Cloud Native Solutions, Containers, Micro services
- Proficient in areas of SOA, data analytics, multi-channel services and events
- Ability to define solutions to solve large scale, complex business problems with high resiliency, scalability, performance and concurrency requirements
- Ability to research and understand new technology solutions and present essential comparisons, pros/cons, and target use cases.
- Adept at IT operations, Agile Methodology, Software Development Lifecycle, and Systems Architectures
- Ability to collaborate with business and technical resources to understand and develop solutions that meet business requirements, translating strategy and objectives when necessary
- Ability to lead discussions in a matrix environment focusing on team and organizational success
- Ability to represent architecture in various architecture forums within the enterprise
- Communicates with impact (written and verbal) and can influence decisions across LOB and technology
- 7-10 years large scale technical delivery experience
- 7-10 years of relevant experience in the software architecture, 2-3 years of financial services experience
- Degree in Computer Science, Software Engineering or equivalent is required.

Desired Skills

- Experience in Digital Banking solutions, Blockchain, Conversational Commerce and Robotic Process Automation
- Involvement in Advanced Data Analytics, including Predictive models leveraging Machine Learning and Natural Language Processing
- Software design and development experience in modern programming languages, in addition to Java.
- Knowledge in Continuous Integration/Deployment and Container automation
- Ability to provide recommendations supported by Alternative Architecture Analysis (AAA) , Proof for Concepts (POC) , Vendor evaluation & other methods that substantiate the proposals and recommendations
- Knowledge and experience with working in an agile SDLC methodology environment
- Self driven, innovative and pushes for high-quality measurable outcomes

Interested? Email careers [at] huntrecht.com

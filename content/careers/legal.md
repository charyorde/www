---
title: "Senior Legal - Real Estate"
date: 2022-12-20T10:08:46+10:00
draft: false
featured: true
listid: accordion-5294-item-7
heading: heading7
jobid: hjid-02-legal
joblevel: Experienced
weight: 7
---

We are in search of a Real Estate Lawyer to join us. You should be able to help clients with real estate matters and documentations. Besides, you should be able to conduct in-depth research and help identify any real estate violations and issues. Moreover, you should also be able to communicate with clients and offer them appropriate legal advice. 

As a Real Estate Lawyer, you should be able to draft and negotiate real estate transactions. Also, you should be able to represent clients in court and at property exhibitions. Your job responsibilities will also include staying up-to-date with the regulatory changes and monitoring property taxes on regular basis. Furthermore, you should be familiar with various real estate concepts and possess strong analytical skills.  

To be able to perform in this job role, you should have a reliable personality and a detail-oriented approach. In addition to this, you should be able to handle stressful situations. A candidate holding decent years of experience in the legal industry will be preferred. 

Responsibilities
- Researching and identifying any legal risks in the real estate documentations 
- Understanding clients’ requirements and advising them accordingly 
- Communicating the laws and regulations required for real estate transactions to the clients 
- Assisting in drafting real estate transactions 
- Negotiating real estate contract terms and conditions 
- Reviewing and preparing leases and amendments 
- Keeping a track on regulatory and compliance-related requirements 
- Representing clients in sale of properties and other exhibitions 
- Ensuring that all property documentations are in line with the property laws and regulations 
- Monitoring property taxes and value estimates 
- Representing clients in court and drafting legal pleadings

Requirements
- Bachelor’s degree in Law or a related field 
- Proven work experience as a Real Estate Lawyer, Lawyer, Legal Advisor , or a similar position in the Legal industry 
- Holding a valid license to practice law 
- Complete knowledge of the laws and regulations governing the real estate industry 
- Familiarity with real estate concepts such as litigation, property management, and sales transactions 
- Strong analytical and critical thinking skills 
- Good time management and organizational skills 
- Excellent communication, presentation, and negotiation skills 
- Highly motivated and detail-oriented individual 
- Ability to maintain confidentiality of any sensitive information 
- Ability to offer excellent customer service

Interested? Email careers [at] huntrecht.com

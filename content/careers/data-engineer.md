---
title: "Data Engineer"
date: 2023-02-090T10:08:46+10:00
draft: false
featured: true
listid: accordion-5294-item-12
jobid: hjid-02-data-engineer
joblevel: Experienced
heading: heading12
weight: 12
---
We are looking for an experienced data engineer to join our team. You will use
various methods to transform raw data into useful data systems and employ
machine learning techniques to create and sustain structures that allow for the
analysis of data while remaining familiar with dominant programming and
deployment strategies in the field. Overall, you’ll strive for efficiency by aligning data systems with business goals. 

To ensure success as a data engineer, you should have strong analytical
skills and the ability to combine data from different sources. Data engineer
skills also include familiarity with several programming languages and knowledge
of learning machine methods.

If you are detail-oriented, with excellent organizational skills and experience
in this field, we’d like to hear from you. 

Responsibilities
- Analyze and organize large, raw and complex sets of data that meet non-functional and functional business requirements
- Build data systems and pipelines
- Build analytical tools to utilize the data pipeline, providing actionable insight into key business performance metrics including operational efficiency and consumer acquisition
- Conduct complex data analysis and report on results
- Prepare data for prescriptive and predictive modelling
- Build algorithms and prototypes
- Combine raw information from different sources
- Explore ways to enhance data quality and reliability
- Identify opportunities for data acquisition
- Develop analytical tools and programs
- Collaborate with data scientists and architects to support their data infrastructure needs while assisting with data-related technical issues

Requirements
- Demonstrated understanding and experience with tools such as Kafka, Spark, HDFS and Hadoop, MongoDB and relational databases
- IBM Certified Data Engineer or Google's Certified Professional Data Engineer is an asset
- Experience with serverless computing
- Understanding of machine learning and operations research
- Knowledge of SQL, Python; familiarity with Scala, Java or C++ is an asset
- Experience with Natural Language Processing models including BERT, GPT-3, Watson, LaMDA, T5 etc
- Experience with Hadoop and HDFS
- Analytical mind and business acumen
- Strong math skills (statistics and algebra)
- Problem-solving aptitude
- Excellent communication and presentation skills
- BSc/BA in Computer Science, Engineering or relevant field; graduate degree in Data Science or other quantitative field

Excellent compensation package with full benefits including health and mortgage

Interested? Email careers [at] huntrecht.com

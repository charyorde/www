---
title: "Web3 Software Engineer"
date: 2025-02-110T10:08:46+10:00
draft: false
featured: true
listid: accordion-5294-item-27
jobid: hjid-02-wswe
joblevel: Experienced
heading: heading27
weight: 27
---

As our Web3 / Full-stack Software Engineer, you will be designing, developing,
and deploying software solutions that power various products at Huntrecht. In
this role, you will work closely with a group of talented engineers, product
managers, and UX designers to build the next generation of staking products.
Your work will help with securing a wide range of blockchain networks and
contribute to the bedrock of decentralization.

Responsibilities
- Build new products and features that meet critical product and business needs
- Build pixel-perfect and responsive web applications with web3.js, ethers.js, React.js and TypeScript
- Continuously prototype and improve UIs iteratively with the product and design team
- Write server code in Python or Javascript that is extensible, well-tested, and secure
- Leverage automated testing, CI/CD, container orchestration, and other modern development techniques
- Collaborate with the team to review PR, and make improvements to the codebase

Qualifications/Requirements
- You have deep front-end development knowledge and knowledge of React 
- You have experience with web3: blockchain, and smart contracts
- You are proficient in using web3.js and ethers.js libraries
- You are knowledgeable with Ethereum and EVM, and Non-EVM concepts
- You have a good understanding of data structures and algorithms
- You have a track record of designing, creating, and maintaining production services
- Familiar with Linux, cloud services (such as AWS), and Docker
- Able to work independently and set up an efficient development environment
- You have basic skills on Unix command line & shell scripting

Interested? Email careers [at] huntrecht.com

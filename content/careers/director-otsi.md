---
title: "Director - Technology, Strategy & Innovation"
date: 2024-10-130T10:08:46+10:00
draft: false
featured: true
listid: accordion-5294-item-22
jobid: hjid-02-director-otsi
joblevel: Executive
heading: heading22
weight: 22
---

We are seeking an experienced Director of Technology, Strategy & Innovation to lead our team. The ideal candidate will be responsible for driving innovation and growth by identifying new opportunities, fostering a culture of innovation, and implementing new processes and technologies that enhance capital efficiency, safety, and sustainability across our operations. This role requires a visionary leader who can leverage emerging technologies and industry trends to maintain our competitive edge while servicingthe oil and gas, financial services and CPG business sectors.

This role combines both the CTO & CIO job functions. You will be working out of
our office in New York and may work two (2) days a week remote.

Responsibilities
- Develop and execute the company’s innovation roadmap and strategy for AI aligned with business objectives, focusing both on digital as well as operational innovation. 
- Plays a coordinating role in developing, formulating and communicating corporate technology strategies and objectives; creating and implementing the strategy, roadmap, resourcing and business cases to drive and deliver one or more of our Strategic Objectives
- Collaborate with internal stakeholders including senior management and operations teams to prioritize projects with highest Expected ROR
- Cultivate partnerships with external technology providers, startups, and academic institutions.
- Lead a team of innovators and collaborate cross-functionally to drive transformative projects.
- Identify and assess new technologies, tools, and methodologies relevant to upstream operations. Sponsor and help execute proofs of concept (POCs) and leverage internal relationships with Operations and Technology teams to scale successful ideas from POC to mass implementation.
- Identify use cases of 5G, IoT, AI, data analytics, and other cutting-edge technologies to improve data driven decision making and capital efficiency. 
- Effectively manage the innovation budget to maximize value.
- Establish metrics that measure the success and impact of innovation initiatives.
- Implement digital solutions that optimize workflows, reduce costs, and improve operational efficiency.
- Champion a culture of continuous improvement and agility within the organization.
- Empower individuals to be creative and innovative by providing an environment to test ideas and quickly apply learnings, establishing a continuous improvement culture to drive long term improvements.
- Ensure intellectual property protection and compliance with industry regulations.
- Promote a culture of inclusivity, diversity, and empowerment within the innovation team. 

Qualifications/Requirements
- Bachelor’s degree in engineering, Computer Science, Information Systems, or relevant field
- Minimum 15 years’ experience in Operations, IT, Finance, or relevant field with minimum 10 years in a leadership role with increasing responsibilities focused on innovation, digital transformation, or organization transformation, preferably within oil and gas or food & beverages sectors
- Demonstrated success in implementing digital transformation initiatives and deploying new technologies at scale
- Strategic thinker with the ability to translate vision into actionable plans and measurable results.
- Experience managing multidisciplinary relationships and driving cultural change towards innovation and continuous improvement
- Knowledge of intellectual property management, cybersecurity, technology commercialization, and regulatory compliance
- Ability to remain curious and open to embracing new ideas and opportunities while investigating emerging technologies and their potential future application
- Ability to lead product development that's cross-functional and useful across multiple business sectors.
- Experience in the payments domain desirable
- Experience in strategy, management or technology consulting

The individual is required to follow all applicable safety precautions. Work is performed almost entirely in a controlled (i.e., inside) environment and does not typicall subject the incumbent to any hazardous/extreme elements; some positions may require regularly moving or transporting items weighing up to 25 lbs. around the office for various needs. The successful candidate must be able to complete all essential physical requirements of the job with or without reasonable accommodation.

Interested? Email careers [at] huntrecht.com

---
title: "Finance & Operations Manager"
date: 2022-12-20T10:08:46+10:00
draft: false
featured: true
listid: accordion-5294-item-6
heading: heading6
jobid: hjid-02-finance
joblevel: Experienced
weight: 6
---

We’re looking for an experienced operations Manager to implement the right processes and practices across the organization.

As an Operations Manager, you’ll help in formulating strategy, improving performance, procuring material and resources and securing compliance. You should be ready to mentor your team members, find ways to increase quality of customer service and implement best practices across all levels.

Ultimately, we’ll trust you to help us remain compliant, efficient and profitable during the course of business. 

Responsibilities
- Ensure all operations are carried on in an appropriate, cost-effective way
- Improve operational management systems, processes and best practices
- Purchase materials, plan inventory and oversee warehouse efficiency
- Help the organization’s processes remain legally compliant
- Formulate strategic and operational objectives
- Examine financial data and use them to improve profitability
- Manage budgets and forecasts
- Perform quality controls and monitor production KPIs
- Recruit, train and supervise staff
- Find ways to increase quality of customer service

Requirements
- Proven work experience as Operations Manager or similar role
- Experience in real estate management is a plus
- Knowledge of organizational effectiveness and operations management
- Experience budgeting and forecasting
- Familiarity with business and financial principles
- Excellent communication skills
- Leadership ability
- Outstanding organisational skills
- Degree in Business, Operations Management or related field

Interested? Email careers [at] huntrecht.com

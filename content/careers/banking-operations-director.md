---
title: "Banking Operations Director"
date: 2023-01-310T10:08:46+10:00
draft: false
featured: true
listid: accordion-5294-item-10
jobid: hjid-04-banking-ops
joblevel: Middle Management
heading: heading10
weight: 10
---
We are conducting a search for a Banking Operations Director for our investment and credit business unit. The Banking Operations Director will be responsible for all daily banking operations, ensuring the efficiency necessary to support the company's long term business objectives.

Experience in back office operations of a quantitative finance firm is an absolute prerequisite for this position

This role will report directly to the COO of the company, and will take part in
executive level discussions including crafting strategy and global decision
making.

Responsibilities
- Closely direct backend operations run efficiently and in accordance with appropriate regulations. Operational responsibility includes ACH and wire transfers, credit card processing, loan processing etc.
- Plan, direct and control banking alliance operations, ensuring that established policies and procedures are followed and delegating responsibilities to staff as needed to meet company objectives.
- Manage current investment and credit operations and products, including management and support of the credit processing platform.
- Work closely with internal departments to offer a variety of products and services to customers and prospects.
- Adhere to compliance and operational risk controls policies and practices and report/correct control weaknesses, compliance breaches and operational defects.
- Develop and maintain relationships with industry groups and organizations to keep abreast of changes in financial services regulations and ensure that changes are implemented in a timely fashion.

Requirements
- Deep industry experience (10-15 years) and professional leadership experience in a financial services setting.
- Very strong knowledge of ACH, Wire transfers, Credit Card operations and risk controls.
- Strong operational background; previous experience in a management role.
- You need not be a technologist, however you must be comfortable working immersed in a technology focused environment.You will be expected to participate in the development of cutting edge technologies.
- Ability to motivate and lead a team, coordinate internal and external resources and achieve measurable results against goals.
- Skill in providing organizational leadership by establishing and communicating and modeling vision and goals.

Excellent compensation package with full benefits including health and mortgage

Interested? Email careers [at] huntrecht.com

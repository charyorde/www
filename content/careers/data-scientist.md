---
title: "Data Scientist"
date: 2023-02-090T10:08:46+10:00
draft: false
featured: true
listid: accordion-5294-item-11
jobid: hjid-02-data-scientist
joblevel: Experienced
heading: heading11
weight: 11
---
We are looking to hire a highly creative data scientist to collect and analyze large amounts of raw information from varying sources to find patterns and build predictive models and address data analytics challenges in our organization, to clean and interpret data and create solutions to overcome challenges, and communicate with interested parties. We will rely on you to build data products to extract valuable business insights.

In this role, you should be highly analytical with a knack for analysis, math
and statistics. Critical thinking and problem-solving skills are essential for
interpreting data. We also want to see a passion for machine-learning and
research.

Your goal will be to help our company analyze trends to make better decisions.

Responsibilities
- Identify valuable data sources and automate collection processes
- Undertake preprocessing of structured and unstructured data
- Analyze large amounts of information to discover trends and patterns
- Build predictive models and machine learning algorithms
- Combine models through ensemble modelling
- Present information using data visualization techniques
- Propose solutions and strategies to business challenges
- Collaborate with engineering and product development teams

Requirements
- Proven experience as a Data Scientist or Data Analyst
- Experience in data mining
- Understanding of machine learning and operations research
- Knowledge of SQL, Python; familiarity with Scala, Java or C++ is an asset
- Experience with Natural Language Processing models including BERT, GPT-3, Watson, LaMDA, T5 etc
- Experience with Hadoop and HDFS
- Analytical mind and business acumen
- Strong math skills (statistics and algebra)
- Problem-solving aptitude
- Excellent communication and presentation skills
- BSc/BA in Computer Science, Engineering or relevant field; graduate degree in Data Science or other quantitative field

Excellent compensation package with full benefits including health and mortgage

Interested? Email careers [at] huntrecht.com

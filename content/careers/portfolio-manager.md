---
title: "Structured Credit Portfolio Manager"
date: 2024-05-290T10:08:46+10:00
draft: false
featured: true
listid: accordion-5294-item-19
jobid: hjid-02-portfolio-manager
joblevel: Experienced
heading: heading19
weight: 19
---

Huntrecht  is seeking a Portfolio Manager to fill a senior leadership role
within the firm’s new Structured Credit Opportunities investment strategy (CLO debt/equity). The PM will participate in all aspects of developing this important strategy for one of the firm’s key growth initiatives. This includes marketing, raising capital, systems implementation and development of investment processes. This PM, in coordination with other senior investment team members, will have primary responsibility for all aspects of CLO investing, including portfolio construction, security analysis, absolute and relative value analysis, document negotiation and trading. The PM will also support Huntrecht's CLO team in the issuance/structuring of new CLO transactions.

Responsibilities
- Perform research on both new issue and secondary market opportunities
- Make buy/hold/sell decisions for a portfolio of CLO debt and equity positions
- Maintain dialogue with CLO managers, including formal due diligence and site visits
- Maintain dialogue with CLO primary arrangers and secondary trading desks
- Generate accurate and comprehensive analysis, modeling and valuation of investments
- Collaborate with the credit research team to analyze and assess underlying credit risks within collateral pools
- Provide written and oral updates on a regular and ad hoc basis to internal and
external constituents

Qualifications
- Bachelor’s degree in finance or related area of study; MBA and/or CFA a plus
- 7-10 years of experience investing, structuring or modeling CLOs
- Prior portfolio management responsibilities highly preferred
- Strong modeling capabilities including in depth knowledge of how to operate off the shelf modeling tools, such as Intex, Kanerai, et al
- Demonstrated ability to review, interpret, evaluate and negotiate deal documentation
- Strong written and verbal communication skills
- Detail oriented work product and efficient output
- Strong ability to multi-task investment, research, marketing and reporting responsibilities
- Must demonstrate an entrepreneurial spirit and be able to work collaboratively across the entire firm

Interested? Email careers [at] huntrecht.com

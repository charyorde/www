---
title: "Senior Architect"
date: 2022-12-20T10:08:46+10:00
draft: false
featured: true
listid: accordion-5294-item-4
heading: heading4
jobid: hjid-02-architect
joblevel: Experienced
weight: 1
---

The Senior Architect assists the Project Manager as a project team member, interacting with project owners, architects, engineers, contractors and subcontractors to develop and analyse the virtual construction process in order to maximize construction efficiency, job cost savings, as well as enhance project documentation & delivery. 

Responsibilities
- Able to perform all BIM Architect responsibilities
- Assist Project Manager in preparing BIM Guidelines for project specific requirements
- Assist Project Manager in training of project personnel in the use of BIM tools
- Assist Project Manager with planning and scheduling, assist in cost report setup and forecasting, issue change orders, notices and revisions.
- Understand contractual and legal guidelines. 
- Assist project team with construction coordination and scheduling.
- Print drawing sets for team review.
- Support Client to calculate quantities for material orders and crew work planning.
- Prepare 3D models and dimension shop drawing sheets for field use and review.
- Export DWF and other formats for Inter-Disciplinary Clash Detection.
- Identify model constructability issues and propose solutions.
- Create job layout driven by 3D model data. 
- Timely and accurate preparation of BIM models, shop drawings, etc.
- Provide daily and weekly timesheets.
- Demonstrate a deep interest in learning new skills and knowledge.
- Positive attitude towards collaboration and knowledge sharing.
- Ability to work both independently as well as part of a team.
- Demonstrate effective communication skills.
- Work with minimal supervision.

Requirements
- A degree in Architecture, Construction Management or equivalent.
- Minimum 5 years project experience, with duties as a lead coordinator – Architect, or equivalent.
- Must be able to demonstrate an intermediate to advanced level of knowledge and understanding of construction methods, processes, and systems.
- Must have knowledge and understanding of common construction terminology and nomenclature as well as various building types and systems.
- Ability to read and understand plans, specifications and BIM modeling.
- Must be able to demonstrate an intermediate to advanced working knowledge and experience with the following software applications as well as typically used terminology, standards, and practices Autodesk Revit, Navisworks, TEKLA, Microstation, etc.
- Demonstrate effective written and verbal English language communication skills.
- Ability to use judgment, problem solving ability, self starting and ability to prioritize based on relevant factors.

Interested? Email careers [at] huntrecht.com

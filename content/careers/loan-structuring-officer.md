---
title: "Loan Structuring Officer"
date: 2024-12-280T10:08:46+10:00
draft: false
featured: true
listid: accordion-5294-item-25
jobid: hjid-02-lso
joblevel: Experienced
heading: heading25
weight: 25
---

As a Loan Structuring & Execution Officer (LS&E), you will play a pivotal role in the structuring, negotiation, documentation, and execution of tailored trade financing for our international customers. This position is integral to our trade financing division, where you will assist with a diverse range of lending transactions, including bilateral, club, and syndicated loans. Your leadership will guide team members in executing these transactions, ensuring that they meet the high standards of quality and excellence that Huntrecht is known for. The LS&E team operates as part of the originating deal team, taking a leading role in structuring and executing lending facilities and financial instruments that encompass Project Finance, Capital Call, and Acquisition Facilities, alongside General Corporate and Working Capital Facilities. This role is not just about executing transactions; it is about enhancing Huntrecht's reputation in the market, improving customer satisfaction, and facilitating increased origination of loans and trade financing. You will be expected to build and maintain productive relationships with both internal and external stakeholders, providing guidance on processes and procedures to your team. In this role, you will lead negotiations with legal and consulting teams to clearly define and document the terms and conditions of credit extensions, ensuring that all security documentation is enforceable and current. You will also be responsible for driving adherence to regulatory and compliance measures, ensuring that all transactions are executed in accordance with the highest standards of governance. Your ability to leverage networks for market intelligence and to influence market terms will be crucial to your success in this position.

Responsibilities
- Structure, negotiate, document, and execute tailored lending facilities for international customers.
- Lead the execution of all documented debt transactions.
- Build and maintain collaborative relationships with internal and external stakeholders.
- Provide guidance on processes and procedures to team members.
- Drive adherence to all regulatory and compliance measures.
- Lead negotiations with lawyers and consultants to define and document terms and conditions of credit extensions.
- Leverage networks to acquire market intelligence and drive opportunities.

Qualifications/Requirements
- Strong leadership and business management skills.
- Highly developed commercial and analytical judgment of transactions and deals.
- Well-developed influencing, networking, and relationship skills.
- Good understanding of complex finance markets.
- Excellent communication skills.
- Tertiary/post graduate qualification or relevant technical qualifications.

Interested? Email careers [at] huntrecht.com

---
title: "Head of Investor Relations"
date: 2024-04-300T10:08:46+10:00
draft: false
featured: true
listid: accordion-5294-item-18
jobid: hjid-02-investor-relations
joblevel: Experienced
heading: heading18
weight: 18
---

The newly appointed Head of IR’s overriding role will be to develop and support
a world class IR programme; one that is recognised by analysts and investors alike. The Head of IR will work closely with the CEO, CFO, the Finance division and the Corporate Communication division to ensure consistent messages are delivered to stakeholders

Responsibilities
- To convey a strong, consistent and well understood investment message
- To put in place a world class investor relations capability, working according to a defined strategy and calendar
- To educate international financial audiences about the Company, including the strength of its corporate strategy and management
- To create the basis for sustained enhancement of the Company’s investor (and other stakeholders’) relationships, through a programme of regular communication to maintain and strengthen the Company’s profile
- To undertake all communications in line with international best practice and relevant market regulation
- To co-ordinate internal processes to ensure effective communications with external audiences
- Continue to develop with the Board, the Company’s key messages and ensure data required by the financial markets are provided in a clear and concise manner on a timely basis, including all regulatory announcements and disclosures
- Work with the external IR advisory consultancy and internal management to ensure that other corporate press releases of a market sensitive nature are produced on a timely basis and to the requisite ‘world class’ standard
- Designing, copywriting and maintaining the investor relations section of the corporate website; and developing other collateral and presentations, including the annual report and investor packs to support analyst/investor outreach
- Competitor and peer-group analyses; shareholder and bond holder identification
- Develop an investor targeting programme and manage the annual calendar of IR conferences and other events to provide regular contact with principal bond- and shareholders and key underweight and non-holders
- Liaison with external bond- and shareholder identification service providers
- Attend key investor/analyst/journalist meetings, where appropriate with CEO and/or CFO and other Executive Directors
- Manage periodic investor days and coordinate ad hoc analyst and investor tours, ensuring supporting materials are prepared as necessary
- Maintain regular contact with buy- and sell-side analysts, including responding to ad hoc queries
- Prepare an annual investor relations plan and budget for the IR function
- Prepare annual shares buyback program
- Monitor analysts’ forecast on an ongoing basis, comparison with the Company’s own forecasts, and work to guide expectations as appropriate
- Monitor and review analyst reports on the Company and its peers and highlight any key issues for discussion with Executive Management
- Run Debt Capital Market IR programme
- Write the investor relations Board reports to ensure Executive Management are up to date with the IR function and the financial markets’ views and concerns
- Maintaining a database of all investor/broker contact
- Create detailed regular (weekly) reports of sell-side and buy-side interaction to Group CFO/CEO
- Liaise with the company’s retained stockbrokers, investment bankers and financial PR advisors
- Liaise with, and represent the Company, in external stakeholder events and programmes (Investor conferences, conference calls, roadshows)

Qualifications
- An in-depth understanding of the financial and investment markets
- Knowledge of and experience in Capital Markets / Corporate IR / Financial Media management
- Excellent communication skills at all levels and within different mediums
- Ability to develop relationships both inside and outside the Company
- Ability to see bigger picture and contribute to any discussion beyond the specific remit of the role
- Proactive and organised, with the ability to work autonomously
- Ability to multi-task and must be well organized; attention to detail; high level of accuracy
- Ability to respond sensitively and appropriately to confidential issues and information
- High level of accuracy, consistency and timely responses are essential
- Flexibility to travel as and when required
- Previous communications experience would be an advantage, acquired either in-house or through a consultancy
- Exceptional buy- and sell-side analysts will also be considered
- Bachelors/Master’s degree with specialization in Finance
- CPA / CIMA / CA / CFA / MBA qualification may be advantages but not a specific requirement
- Vocational certification such as Certified Practitioner of Investor Relations (CPIR)/ Certificate in IR (CIR/CIRO) or similar would be an advantage

Interested? Email careers [at] huntrecht.com

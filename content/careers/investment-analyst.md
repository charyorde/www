---
title: "Investment Analyst"
date: 2024-04-190T10:08:46+10:00
draft: false
featured: true
listid: accordion-5294-item-15
jobid: hjid-02-investment
joblevel: Intermediate
heading: heading15
weight: 15
---

We are looking for an experienced Investment Analyst to undertake financial
research and report on prospective investment avenues. The successful candidate
will study how an investment is likely to perform and how sustainable it is. You
will participate in our sound portfolio-management strategy by providing the
necessary data for our decision making process.

Responsibilities
- Examine and assess economic and market trends, earnings prospects, financial statements and various other indicators and factors to determine suitable investment strategies
- Reporting discrepancies and issues to senior team leaders when they arise.
- Look back at previous investment decisions and the thought process of making the investment decision
- Liaise with fund managers and network with industry professionals
- Conducting meetings with clients and management throughout the year.
- Monitor closely financial press and keep a track of market trends, opportunities, risks and new investment products
- Compile advisory reports and make informed recommendations on new investment opportunities and funds that could enhance or diversify portfolios
- Develop complex financial models and analyse legal documents

Qualifications
- Proven working experience as an Investment Analyst
- Solid understanding of financial set-up, procedures, statistics and economics
- Excellent investment analysis software user
- Strong numeric skills
- Excellent quantitative and qualitative analytical skills
- BS degree in Finance, Accountingi, investment banking or related field
- Proficiency in data collection and analysis
- Excellent research and writing abilities.
- Thorough understanding of financial analysis and investment strategy
- Interpersonal skills and the ability to work under pressure

Interested? Email careers [at] huntrecht.com

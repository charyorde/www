---
title: "Credit & Collections Manager"
date: 2023-26-260T10:08:46+10:00
draft: false
featured: true
listid: accordion-5294-item-14
jobid: hjid-02-collections
joblevel: Experienced
heading: heading14
weight: 14
---

Our innovative and growing company is looking to fill the role of experienced Credit & Collections manager. As a Credit & Collections manager, you will be responsible for managing the credit and collections activities of our organization. The primary goal is to ensure timely and efficient collection of outstanding payments while maintaining strong customer relationships.

Responsibilities
- Systemically intake, escalate, and triage receivables (collections and cash payments) contacts and issues and follow-through to resolution, including executive-level communication.
- Constant interaction with customer procurement teams to understand the pre-requisites documentation, Accounts payable process to ensure timely payment of invoices
- Review of unapplied balances to initiate discussions with customer finance team to understand invoice payment and applicable deductions in a timely manner
- Responsible for ensuring that any communication directed at site level by customers is directed to the right stakeholders regarding remittance advices or dispute claims in a timely manner
- Collaborate with Customer Facing Specialist, inside sales, business development and sales managers at various locations
- Research and provide insight into legislative issues like tax, changes and potential transactions that could impact the business and its objectives
- Assist management team to prepare for presentations where required, provide information to regulatory requests for information at bidding and contracting phases
- Prepare and review monthly cash forecast with the management
- Recognize escalation trends and identify underlying defects and root causes
- Assess problem accounts and involve management for resolution
- Lead the account reconciliation process through deep dives and working directly with clients and timelines for late stage receivables ensuring steady progress toward timely resolution
- Create, validate and implement operational focused dashboards that measure both cash and/or process improvements
- Approve quotes and Sales Orders
- Initiate credit memos/refunds/adjustment in order to maintain AR aging integrity as needed
- Assist Customer Service department on customer issues relating to credit and collections
- Provide invoices to customers upon request

Qualifications
- Desired: Previous experience and knowledge of Salesforce
- 5+ years of tax, finance or a related analytical field experience
- 5+ years of Accounts Receivable or Account Payable experience
- 5+ years of business-to-business collections experience
- 5+ years of solving complex business challenges using data analysis and modeling
- Advanced Excel skills including but not limited to: pivot tables and charts, VLOOKUP, HLOOKUP, Index/Match/Match, Sumifs, nested formulas, filters, basic functions, tables
- Great communicator: you have the capacity to listen to and collect feedback from a variety of stakeholders, ensuring that everyone is working toward the same goal
- You lead by example by being humble but ambitious, with a desire to learn more

Interested? Email careers [at] huntrecht.com

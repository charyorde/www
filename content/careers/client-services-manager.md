---
title: "Client Services Manager"
date: 2023-02-160T10:08:46+10:00
draft: false
featured: true
listid: accordion-5294-item-13
jobid: hjid-02-client-services-mgr
joblevel: Experienced
heading: heading13
weight: 13
---
We are hiring an experienced Client Services Manager to help us keep growing. If
you're a professional and courteous client services manager with great people skills to help our business prioritize existing customers and bring new clients on board. You will be tasked with maintaining current clients and growing our customer base, keeping accurate correspondence records, meeting regularly with management, updating client details, developing customer service policies and recommending different product options to clients.

To ensure success in this role, the ideal candidate should demonstrate excellent
active listening and communication skills, good personal presentation, politeness, and tact, and be able to function in a high-pressure environment. The noteworthy client services manager should provide timely solutions to clients’ problems, build sustainable and continuous relationships with clients, show initiative and drive when dealing with client requests, and assist in developing marketing material and sales strategies.

Responsibilities
- Resolve complex client problems or disputes in a professional manner
- Keep records and documentation of client interactions for training purposes
- Create monthly and quarterly departmental reports to determine whether KPIs are being met and where there is room for improvement
- Inspiring repeat business from clients
- Conducting customer service workshops and presentations.
- Participating in marketing campaigns.
- Dealing with client requests and troubleshooting problems.

Requirements
- 5+ years prior experience in customer facing roles 
- Excellent communication skills and the ability to anticipate the needs of customers
- Should possess strong problem solving skills and the ability to make sound judgement calls
- Innovative, creative thinking skills to ensure the organization is providing a cutting edge client experience
- Superior organizational and time management skills
- Innovative, creative thinking skills to ensure the organization is providing a cutting edge client experience
- Knowledge of customer service programs and databases, or the ability to learn new software quickly

Excellent compensation package with full benefits including health and mortgage

Interested? Email careers [at] huntrecht.com

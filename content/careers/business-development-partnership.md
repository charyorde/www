---
title: "Partnerships and Business Development Manager"
date: 2024-11-250T10:08:46+10:00
draft: false
featured: true
listid: accordion-5294-item-23
jobid: hjid-02-bdmp
joblevel: Experienced
heading: heading23
weight: 23
---

The Partnerships and Business Development Manager will drive business development activities, significantly contributing to the development of new programme partnerships, product and project opportunities. You are expected to develop and implement market strategies including partnerships and new business alliances. Establishing and managing relationships, both internally and externally is a key element of this job.

Responsibilities
- Work flexibly across Business Development & Marketing and Programme teams, and projects where required
- Build strategic partnerships and alliances, including corporate funders
- Take a lead role in identifying and supporting the development of relationships with key stakeholders that are strategically important partners
- Investigate and contribute to strategic decisions in relation to developing alternate income streams
- Support the development of new and current partnerships by identifying and building relationships with propective partners on bids
- Ensuring that background checks and due diligence for potential partners are completed at an appropriate level relative to risk levels
- Manage tender and speculative deadlines an ensure their delivery
- Actively involved in the planning and presenting of tender documents, ensuring high quality bid writing
- Make recommendations on which tenders should be pursued following the Go/No Go process.
- Oversea the co-ordination, development, completion and submission of tender documents
- Manage consultants hired to co-ordinate, lead and support project development and biding processes.
- Develop and maintain appropriate business development tools and processes as needed
- Any other responsibilities as may reasonably be required from time to time

Qualifications
- Bachelor's degree in business, management, finance or a related field.
- Minimum of 3 years relevant experience at a decision-making level
- Experience in project management, direct line management responsibilities and complex project management
- Demonstrable expertise in writing funding proposals, responding to tenders and report writing
- Strong financial management skills, ability to accurately cost developments and experience in committing to and tracking schedules and budgets

Interested? Email careers [at] huntrecht.com

---
title: "Product Manager"
date: 2022-12-20T10:08:46+10:00
draft: false
featured: true
listid: accordion-5294-item-2
heading: heading2
jobid: hjid-02-product
joblevel: Experienced
weight: 1
---

We are looking for a Product Marketing Manager to lead our outbound marketing activities. You’ll be responsible for presenting our products in ways that will strengthen our brand and boost sales.

For this role, you be a creative and quantitative thinker. You should be familiar with various product marketing techniques, like email campaigns and pricing strategies.

Your goal will be to develop and implement the most profitable plans to position and promote our products.

Responsibilities
- Study company products
- Translate technical details into benefits for the user
- Follow and analyze market trends to position products
- Develop product marketing strategies (pricing, advertising, product launching)
- Craft compelling messages across marketing channels (landing pages, ad campaigns)
- Work with various teams (design, content, acquisition, product, sales) to implement strategies
- Test marketing product features, releases and ad copy
- Evaluate projects using relevant KPIs and feedback from existing and prospective customers

Requirements
- Proven experience as a Product Marketing Manager, Social Media Marketing or similar role
- Background in design and copywriting is a plus
- Experience in market analysis
- Familiar with product marketing tactics (e.g. integrated marketing campaigns)
- Working knowledge of web analytics tools (Google Analytics, WebTrends)
- Working knowledge of marketing tools such as SendGrid, MailChimp
- Excellent communication skills
- Keen eye for detail
- Creativity
- Good writing skills with Blogging experience
- Analytical mind and strong quantitative skills
- BSc/BA in Marketing, Communications or similar field

Interested? Email careers [at] huntrecht.com

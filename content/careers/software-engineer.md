---
title: "Software Engineer"
date: 2025-02-110T10:08:46+10:00
draft: false
featured: true
listid: accordion-5294-item-26
jobid: hjid-02-swe
joblevel: Experienced
heading: heading26
weight: 26
---

We are a fast-growing company with multiple development projects across
technologies and industries. We are looking for team players who are smart,
experienced and who can apply the principles of software engineering to design,
develop, testing, maintenance and evaluation of computer software.

Successful candidates  for this position will have a strong background in
designing and developing world-class microservices and event-driven applications in Java, C++, C#, Python, NodeJS, Go or Rust. Equally important to these specific candidate skills are excellently written and verbal communication skills both with the team and external teams, the ability to multitask, the ability to translate business requirements to engineering requirements, the ability to adapt quickly to new environments, and using creativity to solve difficult problems in a world-class engineering team.

Responsibilities
- Determine operational feasibility by evaluating analysis, problem definition,
    requirements, solution development and proposed solutions.
- Develops software solutions by studying information needs; conferring with
    users; studying systems flow, data usage, and work processes; investigating
    problem areas; following the software development lifecycle. 
- Document and demonstrate solutions by developing documentation, flowcharts,
    layouts, diagrams, charts, code comments, and clear code.
- Prepare and install solutions by determining and designing systems
    specifictions, standards and programming.
- Update technical skills by studying state-of-the-art development tools,
    programming techniques, and computing equipment.
- Be aware of security regulations and carry out all work within the security
    framework of the company and client.

Qualifications
- Programming experience with at least one modern language such as Java, C/C++,
    Python including object-oriented design
- Bachelor's degree in computer science or a related field
- 1+ years experience writing production code of increasing complexity
- Understanding of computer science fundamentals, including algorithms,
    complexity analysis, data structures, problem-solving and object-oriented
    analysis and design.
- Working knowledge of programming languages other than Java, C/C++, Python or Rust
- Working knowledge of databases, service-oriented architectures and
    highly-scaled distributed systems
- Excellent written and verbal communication 
- Experience with Amazon Web Services or any of the Cloud platforms
- Experience wth mission critical, 24x7 systems experience with high throughput
    cloud systems
- Relentless customer focus
- Excellent analytical skills
- Experience implementing and consuming large scale web services.
- Masters degree in Computer Science or equivalent.

Interested? Email careers [at] huntrecht.com

---
title: "Sales Executive(B2B Payments)"
date: 2024-06-120T10:08:46+10:00
draft: false
featured: true
listid: accordion-5294-item-20
jobid: hjid-02-sales-executive
joblevel: Experienced
heading: heading20
weight: 20
---

Huntrecht is building cross-border payments infrastructure that provides
businesses access to global markets, covering FX and trade financing, delivered
as Managed Service. We are enabling businesses to reduce risk of bulk payments and increase access to liquidity for CAPEX in a simple, reliable, compliant, transparent and financially compelling manner by leveraging a subscription model.

As a Sales executive, you will work alongside our growing team of industry
experts. Your role will be to deliver new deals to our large and growing
database of retailers through building new partnerships with businesses, both
through telesales and field sales. You will  successfully build rapport, gain
buy-ins, pitch, negotiate and deliver excellent customer service. You will be
required to resolve any queries that may arise in the application process and
monitor the application to a speedy completion.

Responsibilities
- Promote and sell our products to new and existing customers through effective sales techniques, presentations, and demonstrations.
- Close sales and sign-up businesses through a tenacious market penetration approach
- Build and maintain strong relationships with clients, understanding their business requirements, and providing solutions to meet their needs.
- Achieve sales targets and objectives by proactively prospecting and closing sales deals.
- Collaborate with the other GTM teams to develop and implement sales strategies, including pricing, discounts, and promotions, to drive revenue growth.
- Keep abreast of product knowledge, industry trends, and competitors to effectively communicate value propositions and differentiate our offerings.
- Prepare and deliver sales proposals, quotes, and contracts, ensuring accuracy and timely follow-up to secure customer commitment.
- Provide regular reports and updates on sales activities, including pipeline status, sales forecasts, and customer feedback.
- Attend industry events, trade shows, and conferences to network with potential clients and promote our products or services.
- Collaborate with the customer service team to address customer inquiries, resolve issues, and ensure high customer satisfaction.

Qualifications
- Bachelor's degree in business, marketing, or a related field (preferred but not required).
- Proven experience of 5-7 years in sales, preferably in a B2B environment.
- Strong sales acumen with a track record of meeting or exceeding sales targets and driving revenue growth across diverse business sectors such as oil & gas, transportation and CPG
- Excellent communication and interpersonal skills, with the ability to build rapport, negotiate, and influence customers effectively.
- Strong problem-solving skills and the ability to understand and articulate customer needs and provide appropriate solutions.
- Self-motivated and results-driven, with the ability to work independently and as part of a team.
- Proficiency in using CRM software and other sales productivity tools to manage leads, track sales activities, and generate reports.
- Ability to adapt to a fast-paced and dynamic sales environment, with a willingness to learn and embrace new sales techniques and technologies.
- Excellent time management and organizational skills to prioritize tasks, manage multiple clients, and meet deadlines.
- A customer-centric approach with a strong focus on delivering exceptional service and building long-term relationships.

Interested? Email careers [at] huntrecht.com

---
title: "Senior Officer, Property Valuation"
date: 2024-07-280T10:08:46+10:00
draft: false
featured: true
listid: accordion-5294-item-21
jobid: hjid-02-property-valuation
joblevel: Experienced
heading: heading21
weight: 21
---

The property valuer plays an important role in determining a property's market
value. Our ideal candidate will provide a fair and balanced evaluation of
property value in our portfolio on a regular basis and will work hand-in-hand
with the Structured Credit Portfolio Manager.

Responsibilities
- Site inspection of properties
- Measurement of plans and cadastral plans
- Photograph interior and exteriors of properties for reference to estimating property value and completing appraisal reports.
- Conduct Survey, inspect and investigate all aspect of building and land properties to identify property characteristics in order to determine their values.
- Determine valuations methods by selecting applicable approach and techniques to valuate properties for both commercial, residential and mixed-use.
- Prepare valuation report, outlining methods used in the appraisal process.
- Assist in indicating property value by providing appraisals and consultants to all branches.
- Obtain real estate industry data, collect, verify and analyse property information regarding socio-economic, governmental and environmental characteristics, restrictions, covenants, contract declarations special assessments, ordinances development litigation.
- Regularly review property market in all provinces and update report
- Study property market and prepare a report for reference in the debt financing process/review
- Finalize property price
- Do planning, organizing, leading and controlling valuation team.

Qualifications
- Bachelor's degree in Business Administration, Finance and Banking, Accounting or a related field (preferred but not required).
- At least 3 years of working experiences.
- Good in English both verbal and written communication is a plus.
- Good at analysing, planning and interpersonal skills.

Interested? Email careers [at] huntrecht.com

---
title: "Executive Assistant"
date: 2022-12-20T10:08:46+10:00
draft: false
featured: true
listid: accordion-5294-item-5
heading: heading5
jobid: hjid-02-ea
joblevel: Experienced
weight: 5
---

We are looking for an Executive Assistant to perform a variety of administrative tasks and support our company’s senior-level managers.

Executive Assistant’s responsibilities include managing calendars, making travel arrangements and preparing expense reports. To be successful in this role, you should be well-organized, have great time management skills and be able to act without guidance.

Ultimately, you will contribute to the efficiency of our business by providing personalized and timely support to executive members. 

Responsibilities
- Act as the point of contact among executives, employees, clients and other external partners
- Manage information flow in a timely and accurate manner
- Manage executives’ calendars and set up meetings
- Make travel and accommodation arrangements
- Rack daily expenses and prepare weekly, monthly or quarterly reports
- Oversee the performance of other clerical staff
- Act as an office manager by keeping up with office supply inventory
- Format information for internal and external communication – memos, emails, presentations, reports
- Take minutes during meetings
- Screen and direct phone calls and distribute correspondence
- Organize and maintain the office filing system

Requirements
- Work experience as an Executive Assistant, Personal Assistant or similar role
- Excellent MS Office knowledge
- Outstanding organizational and time management skills
- Familiarity with office gadgets and applications (e.g. e-calendars and copy machines)
- Excellent verbal and written communications skills
- Discretion and confidentiality
- PA diploma or certification is a plus

Interested? Email careers [at] huntrecht.com

---
title: "Account Manager"
date: 2022-12-20T10:08:46+10:00
draft: false
featured: true
listid: accordion-5294-item-1
heading: heading1
jobid: hjid-02-accounts
joblevel: Experienced
weight: 1
---

We are looking for an accountant to manage all financial transactions, from fixed payments and variable expenses to bank deposits and budget.

Accountant responsibilities include auditing financial documents and procedures, reconciling bank statements and calculating tax payments and returns. To be successful in this role, you should have previous experience with bookkeeping and a flair for spotting numerical mistakes.

Ultimately, you will provide us with accurate quantitative information on financial position, liquidity and cash flows of our businesses, while ensuring we’re compliant with all tax regulations.

Responsibilites
- Manage all accounting transactions
- Prepare budget forecasts
- Publish financial statements in time
- Handle monthly, quarterly and annual closings
- Reconcile accounts payable and receivable
- Ensure timely bank payments
- Compute taxes and prepare tax returns
- Manage balance sheets and profit/loss statements
- Report on the company’s financial health and liquidity
- Audit financial transactions and documents
- Reinforce financial data confidentiality and conduct database backups when necessary
- Comply with financial policies and regulations
- Manage and co-ordinate sales logistics
- Act as office manager and co-ordinate office activities and manage teams

Requirements
- 1-2 years work experience as an Accountant
- Excellent knowledge of accounting regulations and procedures, including the Generally Accepted Accounting Principles (GAAP)
- Hands-on experience with accounting software like Odoo, FreshBooks and QuickBooks
- Advanced MS Excel skills including Vlookups and pivot tables
- Experience with general ledger functions
- Strong attention to detail and good analytical skills
- BSc in Accounting, Finance or relevant degree
- Additional certification (CPA or CMA) is a plus

Interested? Email careers [at] huntrecht.com

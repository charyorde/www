---
title: "Head of Trading"
date: 2024-04-190T10:08:46+10:00
draft: false
featured: true
listid: accordion-5294-item-16
jobid: hjid-02-trading
joblevel: Experienced
heading: heading16
weight: 16
---

You will be responsible for generating revenue, increasing profitability and
market share within the limits of the risk management policy of the Company. The Head will manage an experienced trading desk, centralizing orders from different business units, and understand risks at different degrees and correlation levels covering the Rates, Credit, Commodities, FX and Regulatory parts.

Responsibilities
- Create, develop and deliver category strategies to achieve sales targets
- Carry out detailed financial evaluation of sales, contribution to profit on exchange and margin for key initiatives
- Develop and lead strategies for potential new sales territories and trading markets
- Full understanding of local and international market dynamics. Knowledgeable of various Global markets products in rates/credit and commodities including other derivatives that can be used to achieve targets without exposing the Company to unnecessary risk
- Work in the pricing, risk management and development of a wide variety of products and operate across different platforms globally.
- Tactical trading in rates, FX and commodities to deliver performance vs budgets, identify and address risk and maximize opportunities. Build on the organization's trading by formulating options or hedging strategies including but not limited to proprietary trading in G10 currencies, manage flows from sales team, monitor currency pairs positions, hedge currency pairs accordingly using different asset classes, monitor correlation in rates/FX/commodities and optimize new trading/hedging opportunities and good charting skillsets.
- Direct compliance to trade controls to safeguard organization's trading positions
- Identify developments in products and maintains business relationships with stakeholders
- Technical analysis including chart study in FX/Commodities/Rates and Elliot Wave analysis
- Training of staff in technical analysis and basic concepts of currency pairs pricing
- Prepare and present strategic papers to risk and other high level committes.
- Ensure adherence to policies & procedures, legal and ethical requirements, audit requirements and established risk guidelines

Qualifications
- BSc and/or postgraduate/Professional Qualification in Finance/Mathematics or any other related field 
- At least 10 years of successful experience in Trading in international markets 
- Ability to lead, coach and mentor teams 
- Excellent analytical and technical skills 
- Excellent organizational, communication, interpersonal and critical thinking skills 
- Strong strategy and business planning skills 
- Strong leadership and people development capabilities 

Interested? Email careers [at] huntrecht.com

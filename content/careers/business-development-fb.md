---
title: "Business Development Manager, Food & Beverages"
date: 2024-04-190T10:08:46+10:00
draft: false
featured: true
listid: accordion-5294-item-17
jobid: hjid-02-bdmfb
joblevel: Experienced
heading: heading17
weight: 17
---

As a member of the Business Development team, you will drive the growth of our customer base. You are expected to strategically identify target customers, problem solve creatively, develop deep relationships with key stakeholders, and drive user acquisition results. You are an innovator who executes at lightning speed, experimenting with new customer acquisition strategies every week. You are a risk taker who excels at experiential and offline marketing channels. You obsess over metrics such as user acquisition costs, retention, and customer lifetime value. This position requires salesmanship, leadership, creativity, and perpetual analysis.

Responsibilities
- Work closely with the Market Head to achieve accelerated growth goals.
- Execute robust and creative strategies to acquire new users in target locations
- Assist in launching new locations and foster their continued growth.
- Generate unique ideas for marketing and sales strategies to grow our user base
- Assist in segmenting the market and identifying the best matches for our business model.

Qualifications
- Proactive
- University degree and previous experience in Sales, PR/Marketing, Communications, Operations/Logistics or related fields
- Passion for food industry
- Food & Beverage experience is essential
- Excellent sales and negotiation skills, proven record of achieving targets
- Strong oral and written English communication skills
- Data-driven and analytical with calculated decision-making
- Hungry, creative and courageous
- Hard-working, self-motivated
- Must be confident speaking in front of groups
- Strong interpersonal skills
- Ability to multitask in a fast-paced, dynamic work environment
- Ability to work independently, as well as in a collaborative team setting
- Ability to execute quickly

Interested? Email careers [at] huntrecht.com

---
title: "Citrix Administrator"
date: 2023-01-130T10:08:46+10:00
draft: false
featured: true
listid: accordion-5294-item-9
jobid: hjid-02-citrix
joblevel: Experienced
heading: heading9
weight: 9
---

Our innovative and growing company is looking to fill the role of citrix engineer. If you are looking for an exciting place to work, please take a look at the list of qualifications below.

Responsibilities
- 3rd level support - responsible for identifying and managing the maintenance requirements for global SCCM, Remote Access, Vmware Horizon, Citrix services, and maintaining a Desktop SOE infrastructure within the organisation
- Responsible for application support/maintenance and development activities for Citrix infrastructure in a global 7 by 24 environment of 150+ applications running on 20+ servers for 650+ users
- Responsible for developing the Disaster Recovery plan for the Citrix infrastructure
- Technical Liaison responsible to coordinate and conduct in-depth knowledge transfer from the various application teams for all project development and implementations slated to be deployed via Citrix Infrastructure in order to provide seamless integration into production and provide best of class software delivery service
- Execute/document at least quarterly failover tests performed on critical components in the environment
- Procurement of necessary hardware, software, and licensing
- Citrix Administrator is primarily responsible to configure, support and administer a Citrix XenApp environment consisting XenApp 4.5
- Support PCI audits and security compliance
- Performance tuning of Citrix Servers
- Administer and provide day to day engineering and operational support for the Citrix servers environment
- Lead effort to develop technical standards to support and operate technologies within the current or future Citrix system platform

Qualifications
- Citrix certification is mandatory
- Extensive experience in Citrix Xenapp environment (6.x, 7.x)
- Strong knowledge Citrix Xenapp Farm Management and Maintenance
- Knowledge and work Experience on Citrix Web Interface, Storefront, Licensing Manager
- Knowledge and work Experience on Access Gateway
- Knowledge on Citrix Studio tool, Citrix Director Tool, reports and maintenance
- Expert understanding of Active Directory
- Day to day handling Citrix Issues

Interested? Email careers [at] huntrecht.com

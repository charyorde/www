---
title: "Real Estate Project Manager"
date: 2024-12-280T10:08:46+10:00
draft: false
featured: true
listid: accordion-5294-item-24
jobid: hjid-02-repm
joblevel: Experienced
heading: heading24
weight: 24
---

We are looking for a highly skilled and experienced Real Estate Project Manager to join our dynamic team. The ideal candidate will be responsible for overseeing and managing all aspects of real estate development projects from inception to completion. This includes coordinating with architects, engineers, contractors, and other stakeholders to ensure projects are completed on time, within budget, and to the highest quality standards. The Real Estate Project Manager will also be responsible for managing project budgets, schedules, and resources, as well as ensuring compliance with all relevant regulations and standards. The successful candidate will have a strong background in real estate development, project management, and construction, with excellent leadership, communication, and problem-solving skills. This role requires a proactive and detail-oriented individual who can effectively manage multiple projects simultaneously and navigate the complexities of real estate development. If you are passionate about real estate and have a proven track record of successfully managing development projects, we would love to hear from you.

Responsibilities
- Oversee and manage all aspects of real estate development projects.
- Coordinate with architects, engineers, contractors, and other stakeholders.
- Ensure projects are completed on time, within budget, and to quality standards.
- Manage project budgets, schedules, and resources.
- Ensure compliance with all relevant regulations and standards.
- Develop and maintain project documentation and reports.
- Conduct site visits and inspections to monitor progress.
- Identify and mitigate project risks and issues.
- Negotiate contracts and agreements with vendors and contractors.
- Communicate project status and updates to stakeholders.
- Lead project meetings and coordinate project activities.
- Develop and implement project plans and schedules.
- Manage project changes and variations.
- Ensure effective communication and collaboration among project team members.
- Provide leadership and guidance to project team members.
- Resolve project-related conflicts and issues.
- Ensure project deliverables meet quality standards and client expectations.
- Monitor project performance and implement improvements as needed.
- Prepare and present project reports and presentations.
- Maintain strong relationships with clients, stakeholders, and partners.

Qualifications
- Bachelor's degree in real estate, construction management, or a related field.
- Minimum of 5 years of experience in real estate development or project management
- Strong knowledge of real estate development processes and practices.
- Proven track record of successfully managing development projects.
- Excellent leadership and team management skills.
- Strong communication and interpersonal skills.
- Ability to manage multiple projects simultaneously.
- Proficiency in project management software and tools.
- Strong problem-solving and decision-making skills.
- Knowledge of relevant regulations and standards.
- Ability to work under pressure and meet deadlines.
- Strong organizational and time management skills.
- Attention to detail and accuracy.
- Ability to negotiate and manage contracts and agreements.
- Experience with budgeting and financial management.
- Ability to conduct site visits and inspections.
- Strong analytical and critical thinking skills.
- Ability to develop and implement project plans and schedules.
- Proactive and self-motivated.
- Ability to build and maintain strong relationships with stakeholders.

Interested? Email careers [at] huntrecht.com

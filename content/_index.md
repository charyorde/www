---
title: 'Huntrecht - Technology & Real Estate Investment'
intro_image: images/undraw_problem_solving_ft81.svg
intro_image_absolute: true # edit /assets/scss/components/_intro-image.scss for full control
intro_image_hide_on_mobile: true
---

# Capital Management for Businesses

## Investment Solutions for well-managed businesses using Quantitative Methods.

---
title: 'Marketplace Technology for Addressing Trade & Supply Chain Challenges'
date: 2020-08-05T08:09:34+07:00
menu: ''
eventDate: 'Aug 26, 2020, 3:00-4:00pm UTC+1'
active: true
repeatable: true
hasArchives: false
weight: 2
externalRegLink: true
link: https://info.hiperdist.com/ibm-marketplace-technology-for-addressing-trade-supply-chain-challenges
challenge: 
- "Dwindling working capital"
- "Budget overruns and production oversupply"
- High cost and complexity of capital projects
- COVID-19 supply-chain disruption
- Extreme volatility in critical items and supplies
- Stockouts of high-demand goods or stockpiles for zero-demand
- Partial loads and capacity constrained warehouses
- Shortages of critical supplies, need for quick supplier onboarding and visibility
- Higher than needed inventory levels and safety stock due to siloed visibility
benefits:
- "Operational efficiency in buyer & seller trade interaction"
- Improving asset productivity
- Capturing real-time, accurate performance and maintenance data at the point of
    occurence
- Controlling costs & Becoming customer-centric
- COVID-19 response through workforce protection, supply chain stability and customer engagement 
speakers:
 - name: Olabanjo Alimi
   bio: 'Head, Corporate Planning & Strategy, Enyo Retail & Supply'
   pic: images/olabanjo-alimi.jpg
   linkedinLink: 'https://www.linkedin.com/in/olabanjo-alimi-4b952345/'
 - name: Kayode Odeyemi
   bio: 'IBM Partner, AI, Data, Cloud'
   pic: images/kayode.jpg
   linkedinLink: 'https://www.linkedin.com/in/kayodeodeyemi'
 - name: Ben Nguku Namale
   bio: 'Africa Warehousing & Distribution Lead, GSK'
   pic: images/ben-namale.jpg
   linkedinLink: 'https://www.linkedin.com/in/ben-nguku-namale-12088a4b/'
 - name: Aanu Agboola
   bio: 'Head of Transport, Lori Systems, Nigeria'
   pic: images/aanu.jpg
   linkedinLink: 'https://www.linkedin.com/in/aanuoluwapo-agboola-a1681883/'
 - name: Francois Lok
   bio: 'CEO, Afriglobal Logistics'
   pic: images/francois-lok.jpg
   linkedinLink: 'https://www.linkedin.com/in/francoise-lok-80b7171b/'
 - name: Ahmed Ghanem
   bio: 'Factoring Operations, Egypt Factors'
   pic: images/ahmed-ghanem.jpg
   linkedinLink: 'https://www.linkedin.com/in/ahmed-ghanem-85883891/'
 - name: Bryan Mezue
   bio: 'CEO, Lifestores Healthcare'
   pic: images/bryan-mezue.jpg
   linkedinLink: 'https://www.linkedin.com/in/bryan-mezue/'
questions:
 - What are the industry tools & solutions being used to address high operational costs and ensure supply chain resiliency?
 - How are supply chain businesses navigating the challenges brought about by COVID-19? How are Suppliers ensuring adherence to SLA while operating at a reduced capacity?
 - How are businesses addressing out of stock events that are affecting their financial performance or ability to provide critical goods?
 - What are the financial instruments available to Suppliers to ensure continuous production and business continuity?
 - What are the challenges in last mile fulfilment across industries such as pharmaceuticals, agriculture and real estate?
 - Is last mile fulfilment a sustainable solution to reduction in inventory stockpiles?
---
Digital transformation involves integrating across multiple digital processs, such as the development of online marketplaces that are fully integrated into supply chains and distribution networks.

COVID-19 has disrupted many organizations models and pure-plays. This calls for
a Reinvention. Digital Reinvention combines multiple digital technologies – including cloud
computing, cognitive computing, mobile and the IoT – to reconceive customer and partner relationships and operations.

Blockchain is a disruptive technology that can help address multiple challenges, inefficiencies and associated lost productivity while increasing operating transparency across an ecosystem

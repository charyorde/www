---
title: 'Managed Cloud as Solution to Latency issues in Africa'
date: 2020-08-04T05:58:34+07:00
menu: ''
placeholderUser: images/placeholder-user.png
weight: 4
eventDate: 'Oct 22, 2020, 3:00-3:30pm UTC+1'
active: true
repeatable: true
hasArchives: false
externalRegLink: false
link: https://event.on24.com
challenge:
- Demand for highly available local storage systems
- Infrastructure and regulations
- Poor operational resiliency
- Low skills & competency
- Terabytes of unstructured data with low technical know-how 
- How fast can we have intelligence about new information flow?
benefits:
- Availability of accurate and up to date information for marketing for more effective customer engagement and increased sales
- Data synchronization for zero downtime data migrations and upgrades
- Improve the bottomline by controlling costs. Move less data
- Enterprise ready. Transactional integrity with very high volume throughput and low latency
- Continuously replicate on-premise data into cloud-enabled infrastructures
speakers:
 - name: Ismail Hashad
   bio: 'Hybrid Cloud Integration - Middle East & Africa, IBM'
   pic: images/ismail-hashad.jpg
   linkedinLink: 'https://www.linkedin.com/in/ismail-hashad-60576739/'
 - name: Kayode Odeyemi
   bio: 'Partner, Technology & Investments'
   pic: images/kayode.jpg
   linkedinLink: 'https://www.linkedin.com/in/kayodeodeyemi'
 - name: Peter Wilkinson
   bio: 'Regional Manager, WANdisco '
   pic: images/peter-wilkinson.jpg
   linkedinLink: 'https://www.linkedin.com/in/peter-wilkinson-78915434/'
 - name: Wale Odeyemi
   bio: 'Executive Head Strategic Marketing, Vodacom'
   pic: images/wale-odeyemi.jpg
   linkedinLink: 'https://www.linkedin.com/in/wale-odeyemi-7a73ba1/'
 - name: Thibault Malezieux
   bio: 'Managing Director, AGS Worldwide Movers'
   pic: images/thibault.jpg
   linkedinLink: 'https://www.linkedin.com/in/thibault-malezieux/'
---

Clients are using Public cloud resources as a disaster recovery (DR) site, setting up replication from on-premises storage infrastructure to the cloud. 

They are using Public cloud resources to do agile application development and to leverage cloud analytics and AI services for deeper insights. They’re transmitting on-premises storage snapshots to the cloud.

Public cloud is being used to air-gap data copies preventing cyber incidences from reaching all an organization’s data and ensuring they can continue delivering their desired outcomes. 

Some applications are just being lifted and shifted to the cloud. Since few, if any modifications are being made to the apps and largely the same IT organization is operating them, they are looking for as much consistency in operations, management, and procedures as possible. What’s more, they are realizing that even though their infrastructure is now being “rented” in the cloud, they are still concerned with tools for controlling efficiency.  

And some clients are maturing to the point they have cloud-native applications built on containers that are portable across and leveraging the full richness of the Hybrid Multicloud. With an eye toward delivering the most consistent experience possible – to both developers and the bottom line – IT administrators strive for consistent API’s, efficiency tools, and security. A portable, open storage software foundation can help. 


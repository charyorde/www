---
title: 'Personalized Wealth Advisory, Management & Alternative Asset Investing'
date: 2020-08-04T05:58:34+07:00
menu: ''
weight: 5
eventDate: 'Nov 09, 2020, 3:00-4:00pm UTC+1'
active: true
repeatable: true
hasArchives: false
externalRegLink: false
link: https://event.on24.com
challenge: 
- High degree of disruption and urgency to transform
- 58% want to receive advice through web or mobile, but only 12% say they do; Of those who do, only 45% felt needs were met.
- 3rd year in a row of languishing customer experience scores for wealth managers
- 62% of millennials do not consistently manage or monitor their retirement assets
benefits:
- "Correct and accurate information at your fingertips"
- Data-led analysis for quick decision-making
- Creating insights into trading partners on good vs bad transactions
- Access to better trade performance insights
- Unique winning strategies for various asset classes
speakers:
 - name: Arun Kant
   bio: 'CEO, Leonie Hill Capital'
   pic: images/arun-kant.jpg
   linkedinLink: 'https://www.linkedin.com/in/arun-kant-11a1587'
 - name: Joshua Hawley
   bio: 'Chairman, Satoshi Systems Ltd, Mauritius'
   pic: images/josh-hawley.jpg
   linkedinLink: 'https://www.linkedin.com/in/joshua-paul-hawley-108/'
 - name: Kayode Odeyemi
   bio: 'IBM Partner, AI, Data, Cloud'
   pic: images/kayode.jpg
   linkedinLink: 'https://www.linkedin.com/in/kayodeodeyemi'
 - name: Noah Pickholtz
   bio: 'Partner, Deshe Labs Hedge Funds'
   pic: images/noah-pickholtz.jpg
   linkedinLink: 'https://www.linkedin.com/in/noahpickholtz/'
 - name: Gregoire Fredet
   bio: 'Vice President, Private Equity at Enko Capital'
   pic: images/gregoire.jpg
   linkedinLink: 'https://www.linkedin.com/in/gr%C3%A9goire-fredet-18268729/'
 - name: Armand Ruiz
   bio: 'Director, Data Science & AI Elite at IBM'
   pic: images/armand-ruiz.jpg
   linkedinLink: 'https://www.linkedin.com/in/armand-ruiz/'
 - name: Wee Teck Lim
   bio: 'Fund Selector and Hedging Specialist'
   pic: images/wee-teck.jpg
   linkedinLink: 'https://www.linkedin.com/in/limweeteck/'
 - name: Chris Chin, CFA, CAIA
   bio: 'Head of Systematic Equity Data Strategy at Tudor Investment Corporation'
   pic: images/chris-chin.jpg
   linkedinLink: 'https://www.linkedin.com/in/clchin/'
---
There’s an opportunity to empower wealth advisors to deliver the personalized
advisory experience that investors increasingly expect.

Investors’ expectations are increasing: 
By using predictive tools and AI, firms like Amazon and Netflix anticipate their needs and offer them what they’re looking for before they even know what that is. In the process, these firms are setting new standards for personalized experiences, creating the expectation that any information, product, or service that customers want will be available in context in their moment of need. Naturally, customers start to expect the same from their investment firms.

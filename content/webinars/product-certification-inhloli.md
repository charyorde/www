---
title: 'Secure Transparent and Traceable Product Certification using IBM Blockchain. Use Case: Inhloli'
date: 2020-08-04T05:58:34+07:00
menu: ''
placeholderUser: images/placeholder-user.png
weight: 3
eventDate: 'Sept 16, 2020, 3:00-3:30pm UTC+1'
active: true
repeatable: true
hasArchives: false
externalRegLink: true
link: http://www.huntrechtnigerialimited.mydmportal.com/Secure-Transparent-and-Traceable-Product-Certification-using-IBM-Blockchain-Use-Case-Inhloli
tags: ["supply chain", "blockchain", "digital transformation"]
challenge:
- "Long process for certification"
- "Long process for conformity testing and issuing of pass/fail report"
- "Buyer incurs cost to secure added assurance"
- "Common repetitive operations"
- Buyer reliance on certification body integrity and processes but vulnerable to
    fabricator / distributor
benefits:
- "Operational efficiency in buyer & seller trade interaction"
- Improving asset productivity
- Capturing real-time, accurate performance and maintenance data at the point of
    occurence
- Controlling costs
- Becoming customer-centric
speakers:
 - name: Johan Louw
   bio: 'Founder/Managing Director, Agugu Business Solutions, South Africa'
   pic: images/johan-louw.jpg
   linkedinLink: 'https://www.linkedin.com/in/johanlouw'
 - name: Kayode Odeyemi
   bio: 'IBM Partner, AI, Data, Cloud'
   pic: images/kayode.jpg
   linkedinLink: 'https://www.linkedin.com/in/kayodeodeyemi'
---

One of the easiest and most effective ways to embrace digital business is to rethink how you manage content. After all, whether we’re talking about contracts or marketing collateral, emails or instant messages, the information contained in common business documents is vital to companies across all industries, sizes and regions. You simply cannot run without it. 

That’s especially true today because content fits into the broader effort around Digital Business Automation (DBA), which is transforming the way work gets done. Using a range of strategies and technology tools, including Robotic Process Automation (RPA), data capture and cognitive intelligence, modern businesses are leveraging machine learning, artificial intelligence, bots and advanced analytics to improve everything from the customer experience to enterprise-resource planning and supply-chain management, workforce management, sales, marketing and more. Along the way, they are increasing productivity, revenues and customer satisfaction.


---
title: 'Digital Asset Custody & Security'
date: 2020-08-05T08:09:34+07:00
menu: ''
eventDate: 'Sept 22, 2020, 3:00-3:30pm UTC+1'
active: true
repeatable: true
hasArchives: false
weight: 5
externalRegLink: false
link: https://event.on24.com
challenge: 
- Successful attackers gain control of the assets by maliciously signing transactions with the private keys
- Lack of regulatory frameworks
- Trust gap between customers and custodians
- Crypto-native firms applying for traditional custodian licenses while still exposed to regulatory risk
benefits:
- "Online wallets with better than offline security, improving asset liquidity"
- Remove human attack vectors from cold storage, transaction approval, and solution administration
- Guaranteed security with IBM enterprise credibility
- Protect against external threats by running in a large protected memory enclave with fully encrypted data at rest & in flight
- Simplified compliance with 100% of your data encrypted at the hardware level
speakers:
 - name: Joshua Hawley
   bio: 'Chairman, Satoshi Systems Ltd, Mauritius'
   pic: images/josh-hawley.jpg
   linkedinLink: 'https://www.linkedin.com/in/joshua-paul-hawley-108/'
 - name: Buchi Nwosu
   bio: 'COO, Pimera Microfinance Bank'
   pic: images/buchi-nwosu.jpg
   linkedinLink: 'https://www.linkedin.com/in/buchi-nwosu-cbap-ccmp-pmp-icp-apm-4878571b'
 - name: Kayode Odeyemi
   bio: 'IBM Partner, AI, Data, Cloud'
   pic: images/kayode.jpg
   linkedinLink: 'https://www.linkedin.com/in/kayodeodeyemi'
 - name: Peter DeMeo
   bio: 'Global Sales Leader - IBM Systems Digital Assets'
   pic: images/peter-demeo.jpg
   linkedinLink: 'https://www.linkedin.com/in/peterdemeo/'
 - name: Kevin Kelly
   bio: 'Co-founder, Delphi Digital'
   pic: images/kevin-kelly.jpg
   linkedinLink: 'https://www.linkedin.com/in/kevin-kelly-cfa-02027b28/'
 - name: Abiodun Adebimpe
   bio: 'Head, Custody at Rand Merchant Bank'
   pic: images/biodun-adebimpe.jpg
   linkedinLink: 'https://www.linkedin.com/in/abiodun-adebimpe-04211023/'
questions:
 - What are security tokens and why are they so important an asset class?
 - How important is a secured digital asset custody to investment firms with exposure to digital assets & cryptocurrencies? 
 - In first ten months 2019, over 380 token offerings have raised a total of USD 4.1bn. Digital asset custody business seems to be ripe for an uptake isn't it?
 - How does institutional managed hardware security module solve private key storage problems compared to self-managed hardware wallets?
 - How important is technology to informing what the regulation of digital asset custody should be?
 - Borrowing and lending using crypto is seeing significant growth. This is creating a need for non-custodial lending which is native to crypto. Are we going to continue to see this trend?
---
Digital Assets are blockchain-based digital representations of **real-world assets like corporate bonds, real estate** or entirely new concepts like cryptocurrencies. Smart contracts are transaction logic that can make previously illiquid asset classes more attractive.

Digital assets represent the creation of a new financial system – one that is more democratic, more efficient, and more vast than anything we have seen. Cybersecurity presents a tough challenge, particularly in the custody of wallets and private keys. 

Financial institutions either work with custodians/trustees or themselves, act as custodians.

#### Private key storage
- Financial institutions need to know who is holding on to — and securing — the private keys.
- Self-custody is essentially a non-starter, primarily due to regulations and the legal obligations that define their fiduciary responsibilities. 
- Self-managed hardware wallets, regardless of how secure, are not a feasible option for them.

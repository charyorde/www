---
title: 'Banking As A Service'
date: 2020-08-05T08:09:34+07:00
menu: ''
placeholderUser: images/placeholder-user.png
eventDate: ''
active: false
repeatable: true
hasArchives: false
weight: 13
externalRegLink: false
link: https://event.on24.com
tags: ["finance", "cloud", "digital transformation"]
challenge: 
- "Repetitive and manual tasks"
- "Identity management"
- "Constantly changing business and regulatory environment"
- "Labor-intensive, ad-hoc analyses which are not reusable and scalable"
- "Root-cause analysis requires deep domain expertise with many variables to take into account"
benefits:
- "Reduced cost and increased operational efficiency"
- "Improved customer engagement across channels"
- "Increase customer satisfaction leading to retention improvement"
- "Improvement in authorization performance and portfolio profitability"
- "Enabling cross-functional teams to deliver business value from data"
speakers:
 - name: Buchi Nwosu
   bio: 'COO, Pimera Microfinance Bank'
   pic: images/buchi-nwosu.jpg
   linkedinLink: 'https://www.linkedin.com/in/buchi-nwosu-cbap-ccmp-pmp-icp-apm-4878571b'
 - name: Kayode Odeyemi
   bio: 'Partner, Technology & Investments'
   pic: images/kayode.jpg
   linkedinLink: 'https://www.linkedin.com/in/kayodeodeyemi'

---

Approval rates of financial transactions have a profound impact on sales, customer experience, and business relationships. Ability to understand the key drivers of poor performance, and proactively detecting and mitigating risks is crucial for business performance and growth.

